-- phpMyAdmin SQL Dump
-- version 4.9.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 13 Okt 2020 pada 02.12
-- Versi server: 10.4.10-MariaDB
-- Versi PHP: 7.3.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `revodb`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `kategori`
--

CREATE TABLE `kategori` (
  `id_kategori` int(10) NOT NULL,
  `nama_kategori` varchar(40) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `kategori`
--

INSERT INTO `kategori` (`id_kategori`, `nama_kategori`) VALUES
(4, 'Cetak Foto Kilat'),
(7, 'Cetak Stiker'),
(8, 'Masker'),
(14, 'Brosur'),
(16, 'Cetak Poster');

-- --------------------------------------------------------

--
-- Struktur dari tabel `produk`
--

CREATE TABLE `produk` (
  `id_produk` int(100) NOT NULL,
  `kategori_id` int(11) NOT NULL,
  `nama` varchar(200) NOT NULL,
  `harga` varchar(200) NOT NULL,
  `deskripsi` varchar(255) NOT NULL,
  `keterangan` text NOT NULL,
  `jenis` varchar(500) NOT NULL,
  `gambar` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `produk`
--

INSERT INTO `produk` (`id_produk`, `kategori_id`, `nama`, `harga`, `deskripsi`, `keterangan`, `jenis`, `gambar`) VALUES
(20, 2, 'asrul', '', '', '', '', '6dd69b089e28fef0ec139c7e3807241e.png'),
(27, 2, 'sasas', '', '', '', '', '82e229ecb1a0ee5018bd0bbe6efa8015.png'),
(28, 2, 'sasas', '', '', '', '', '91c30689c50e3932efd5084a21af91e9.png'),
(29, 2, 'sasas', '', '', '', '', 'd5b39cccc177260416c0aee3ebcc27d0.png'),
(30, 2, 'sasas', '', '', '', '', '0a52ad16032b5fddc41e8493b28ee2f1.png'),
(33, 2, 'sasas', '', '', '', '', 'e8dcb5031f11657f6e5db83c4dff28b8.png'),
(34, 2, 'sasas', '', '', '', '', '3c31c8c72e0cd6e49a750cdcaaa5aabc.png'),
(35, 2, 'sasas', '', '', '', '', '5aa76a91445af36c04505d2bec8daabe.png'),
(36, 2, 'sasas', '', '', '', '', '698663c5fc7653c3bdf452c911d1e9ed.png'),
(37, 2, 'sasas', '', '', '', '', 'a41be313deed71345e3a505fe5fe8de9.png'),
(38, 2, 'Asrul aji', '', '', '', '', '90559e7b4de6f0627fa052ad9c8c41f5.jpg'),
(39, 7, 'Stiker A3', '12.000', 'Lorem', '', 'Stiker dengan ukuran A3', 'e598a4258543d4b23ed0069cad0520e7.png'),
(40, 7, 'Stiker A3', '1.500', 'Lorem', '', 'Stiker dengan ukuran A3', '9f284d5c1dbfcc2716d051dc7d3099a9.png'),
(41, 7, 'Stiker Produk', '1.000', 'Loremn', '', 'Kertas hvs', 'c948f2424e8a8e4cff09e5ef6bbecdf1.png'),
(43, 7, 'Stiker Custom', '2.000', 'lorem', '', 'Stiker', '931cdc959b9c85ec05f7bd09cf9b8e75.png'),
(44, 8, 'Masker New Normal', '10.000', 'lorem', '', 'example', '179011bad421e622c42c86f56097101a.jpg'),
(45, 8, 'Face shield ', '15.000', 'Lorem ipsum', '', 'Lorem', '191eac2c87b9aee64b1e7f82c0983482.jpg'),
(46, 8, 'Face shield untuk wanita', '10.000', 'lorem', '', 'Face shield', 'efc4e496623fed368371217f5acdf0e0.jpg'),
(47, 14, 'Brosur ukuran A4', '5.000', 'lorem', '', 'brosur iklan', '3fca996117214fbaf9e1e801c415511d.png'),
(49, 14, 'Brosur A4', '2.500', 'lorem', '', 'Brosur pamflet', '9d640e384b9fe14e0103d5e776d55f1e.png'),
(50, 14, 'Brosur pemasaran', '10.000', 'lorem', '', 'Brosur iklan', 'd0b6296d0d5d23d2823b428766b075d7.png'),
(51, 14, 'Brosur tipis', '1.000', 'lorem', '', 'lorem', '5d497758431c715517edfcb2c4669eb9.png'),
(52, 16, 'Poster ', '12.000', 'lorem', '', 'lorem', '0cb7d0496ccf66794e40556dce5f54b6.jpg'),
(53, 16, 'Poster A4', '10.000', 'lorem', '', 'lorem', 'e69e20438e722b0182a20a11110e3a49.png'),
(54, 4, 'Cetak foto 3x4', '5.00', 'lorem', '', 'Cetak kilat', 'aff668595434ce7667c00e52ee768807.png'),
(55, 4, 'Cetak Foto kartu', '3.000', 'lorem', '', 'Cetak kilat', '3ecdc7de12a416f8f78c1018e7b49ac8.png'),
(56, 4, 'Cetak Foto dinding large', '50.000', '`lorem', '', 'cetak foto dan figura', 'b55198c55f1707750fbaecf7a40144c7.png');

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(10) NOT NULL,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(255) NOT NULL,
  `role` varchar(10) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `password`, `role`) VALUES
(1, 'Eko Prasetyo', 'eko@gmail.com', '123', '2'),
(2, 'asrul', 'asrul@gmail.com', '$2y$10$eA6VACZK62VazcmFGHITieFfZcrMZQ9Ecd1tl29kFLZHcvtDhsZA6', '2'),
(3, 'asrulaji', 'asrull@gmail.com', '$2y$10$OZPUife0smnHB3VcTOg95OK4l7atkYrnkrUfxQlso6UmR3IP6gmIi', '2');

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `kategori`
--
ALTER TABLE `kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indeks untuk tabel `produk`
--
ALTER TABLE `produk`
  ADD PRIMARY KEY (`id_produk`),
  ADD KEY `kategori_id` (`kategori_id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `kategori`
--
ALTER TABLE `kategori`
  MODIFY `id_kategori` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT untuk tabel `produk`
--
ALTER TABLE `produk`
  MODIFY `id_produk` int(100) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=57;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(10) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
