<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Produk extends CI_Controller {

	public function index()
	{
		// $content = file_get_contents('http://localhost/Magang/rest-server/index.php/produk');
		$content = file_get_contents('http://localhost/Magang/rest-server/index.php/api/produk');
		$array_data = json_decode($content);
		$data['result'] = $array_data->data;
    	$this->load->view('template/admin/header');
		$this->load->view('admin/produk',$data);
		$this->load->view('template/admin/footer');
	}
}
