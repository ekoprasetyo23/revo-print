<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Kategori extends CI_Controller {

	public function index()
	{
		// $content = file_get_contents('http://localhost/Magang/rest-server/index.php/kategori');
		$content = file_get_contents('http://localhost/Magang/rest-server/index.php/api/Kategori');
		$array_data = json_decode($content);
		$data['result'] = $array_data->data;

    	$this->load->view('template/admin/header');
		$this->load->view('admin/kategori',$data);
		$this->load->view('template/admin/footer');
	}
}
