<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Produk extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data produk
    function index_get($id=null) {
        $this->db->select('*');
        $this->db->from('produk');
        $this->db->join('kategori', 'kategori.id_kategori = produk.kategori_id');
        
        if ($id == '') {
            // query tersebut untuk menjoin/mengguakan 2 table dengan menggambil semua data
            $produk = $this->db->get()->result();
        } else {
            // query tersebut untuk menjoin/mengguakan 2 table dengan menggambil sesuai id
            // mengambil data spesifik sesuai id_produk yg dilempar parameter
            $this->db->where('id_produk', $id);
            $produk = $this->db->get()->row();
        }
        if (empty($produk)) {
            $this->response(array('status'=>'empty'), 200);
        }else {
            $this->response(array('status'=>'success', 'data'=>$produk), 200);            
        }
    }


    //Mengirim atau menambah data produk baru
    function index_post() {

        $data = array(
            'kategori_id'      => $this->post('kategori_id'),
            'nama'    => $this->post('nama'),
            'keterangan'    => $this->post('keterangan'),
            'deskripsi'    => $this->post('deskripsi'),
            'jenis'    => $this->post('jenis'),
            'harga'   => $this->post('harga'),
        );

        // upload gambar
        if (!empty($_FILES['gambar']['name'])) {
            $config['upload_path']          = './gambar_produk/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 10000;
            $config['max_width']            = 6000;
            $config['max_height']           = 6000;
            $config['encrypt_name']         = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('gambar')){
                $msg = $this->upload->display_errors();
                return $this->response(array('status'=>'fail','msg'=>$msg),502);
            }else{
                $uploaded = $this->upload->data();
                $data['gambar'] = $uploaded['file_name'];
            }
        }else {
            $data['gambar'] = '';
        }

        // response postman
        $insert = $this->db->insert('produk', $data);
        if ($insert) {
            $this->response(array('status'=>'success','msg'=>'sukses memasukan data', 'data'=>$data),200);
        } else {
            $this->response(array('status' => 'fail'), 502);
        }
    }

     //Memperbarui data prooduk yang telah ada
    function index_put() {
        $id = $this->put('id_produk');

        $data = array(
            'kategori_id'      => $this->put('kategori_id'),
            'nama'    => $this->put('nama'),
            'keterangan'    => $this->put('keterangan'),
            'deskripsi'    => $this->put('deskripsi'),
            'jenis'    => $this->put('jenis'),
            'harga'   => $this->put('harga'),
        );
        
        if (!empty($_FILES['gambar']['name'])) {
            $config['upload_path']          = './gambar_produk/';
            $config['allowed_types']        = 'gif|jpg|png|jpeg';
            $config['max_size']             = 10000;
            $config['max_width']            = 6000;
            $config['max_height']           = 6000;
            $config['encrypt_name']         = TRUE;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('gambar')){
                $msg = $this->upload->display_errors();
                return $this->response(array('status'=>'fail','msg'=>$msg),502);
            }else{
                $uploaded = $this->upload->data();
                $data['gambar'] = $uploaded['file_name'];
            }
        }

        $this->db->where('id_produk', $id);
        $update = $this->db->update('produk', $data);
        if ($update) {
            $this->response(array('status'=>'success','msg'=>'sukses update data', 'data'=>$data),200);
        } else {
            $this->response(array('status' => 'fail'),502);
        }
    }

    function index_delete() {
        $id = $this->delete('id_produk');       
        $this->db->where('id_produk', $id);
        $delete = $this->db->delete('produk');
        if ($delete) {
            $this->response(array('status'=>'success','msg'=>'sukses hapus data'),200);
        } else {
            $this->response(array('status' => 'fail'),502);
        }
    }
}
?>