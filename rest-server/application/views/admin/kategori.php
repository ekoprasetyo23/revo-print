

<div class="container">
    <h4 class="mt-3 text-center">Tambah Kategori</h4>
    <div class="text-left">
        <button style="margin-bottom: 10px;" type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> Tambah </button>
    </div>
    <table id="table" class="display" border="1">
        <thead>
            <tr class="text-center">
                <th width="10%">No</th>
                <th class="text-left">Nama</th>
                <th width="20%">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no =1;
            foreach ($result as $value) :?>
            <tr class="text-center">
                <th scope="row"><?= $no++?></th>
                <td class="text-left"><?= $value->nama_kategori ?></td>
                <td>
                    <button class=" btn btn-info" onclick="show(<?= $value->id_kategori?>,'<?= $value->nama_kategori?>')"><i class="fa fa-eye"></i> </button>
                    <button class=" btn btn-warning" onclick="edit(<?= $value->id_kategori?>,'<?= $value->nama_kategori?>')"><i class="fa fa-pencil"></i> </button>
                    <button class=" btn btn-danger" onclick="hapus(<?= $value->id_kategori?>,'<?= $value->nama_kategori?>')"><i class="fa fa-trash"></i> </button>
                </td>
            </tr>
            <?php endforeach ;?>
        </tbody>
    </table>


    <!-- Modal Insert dan update -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title-add">Tambah Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="kategori_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama_kategori">Nama</label>
                            <input type="hidden" name="id_kategori" id="id_kategori">
                            <input type="text" class="form-control" name="nama_kategori" id="nama_kategori">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="deletemodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">Hapus kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="delete_form">
                    <div class="modal-body">
                        <p>Apakah yakin mengapus kategori <strong id="nama_kategori_delete"></strong> ?</p>
                        <input type="hidden" name="id_kategori" id="id_kategori_delete">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary">Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>


    <!-- modal show data detail -->
    <div class="modal fade" id="showdata" tabindex="-1" role="dialog" aria-labelledby="showdata" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">Detail Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="modal-body">
                        <h4> <strong id="nama_kategori_show"></strong></h4>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                    </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $('#table').dataTable();
    } );
</script> 

<script type="text/javascript">

  // <!-- fungsi input kategori -->
$('#kategori_form').submit(function(e) {
    var form = $(this);
    var url =  "<?= base_url('index.php/api/kategori');?>";
    var types="";
    if ($('#id_kategori').val() == '') {
        types = "POST";
    }else{
        types = "PUT";
    }
    e.preventDefault();
    $.ajax({
        type: types,
        url: url,
        data: form.serialize(),
        dataType: "json",
        success: function(data){
          location.reload();
        },
        error: function() { alert("Error data."); }
    });
});

function edit(id, nama){
    $('#modal-title-add').text('Edit Kategori');
    $('#exampleModal').modal('show')
    $('#id_kategori').val(id);
    $('#nama_kategori').val(nama);
}

function hapus(id, nama){
    $('#deletemodal').modal('show')
    $('#nama_kategori_delete').text(nama);
    $('#id_kategori_delete').val(id);
}

function show(id, nama){
    $('#showdata').modal('show')
    $('#nama_kategori_show').text(nama);
}

$('#exampleModal').on('hidden.bs.modal', function () {
    $('#modal-title-add').text('Tambah Kategori');
    $('#id_kategori').val('');
    $('#nama_kategori').val('');
})

$('#delete_form').submit(function(e) {
    var form = $(this);
    var url =  "<?= base_url('index.php/api/kategori');?>";
    e.preventDefault();
    console.log();
    $.ajax({
        type: "DELETE",
        url: url,
        data: form.serialize(),
        dataType: "json",
        success: function(data){
          location.reload();
        },
        error: function() { alert("Error data."); }
    });
});


</script>