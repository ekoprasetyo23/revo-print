

<div class="container">
    <h4 class="mt-3 text-center">Tambah Produk</h4>
    <div class="text-left">
        <button style="margin-bottom: 10px;" type="button" class="btn btn-primary mt-3" data-toggle="modal" data-target="#exampleModal"><i class="fa fa-plus"></i> Tambah </button>
    </div>
    <table id="table" class="display" border="1">
        <thead>
            <tr class="text-center">
                <th width="10%">No</th>
                <th class="text-left">Gambar</th>
                <th scope="col">Kategori</th>
                <th scope="col">Nama</th>
                <th scope="col">Harga</th>
                <th scope="col">Jenis</th>
                <th width="20%">Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php
            $no =1;
            foreach ($result as $value) :?>
            <tr class="text-center">
                <th scope="row"><?= $no++?></th>
                <td><img style="width:120px" src="<?=base_url()?>./gambar_produk/<?= $value->gambar ?>"></td>
                <td class="text-left"><?= $value->nama_kategori ?></td>
                <td><?= $value->nama ?></td>
                <td><?= $value->harga ?></td>
                <td><?= $value->jenis ?></td>
                <td>
                    <button class=" btn btn-info" onclick="show(<?= $value->id_produk?>,'<?= $value->nama?>','<?= $value->harga?>','<?= $value->jenis?>')"><i class="fa fa-eye"></i> </button>
                    <button class=" btn btn-warning" onclick="edit(<?= $value->id_produk?>,'<?= $value->nama?>')"><i class="fa fa-pencil"></i> </button>
                    <button class=" btn btn-danger" onclick="hapus(<?= $value->id_produk?>,'<?= $value->nama?>')"><i class="fa fa-trash"></i> </button>
                </td>
            </tr>
            <?php endforeach ;?>
        </tbody>
    </table>


    <!-- Modal Insert dan update -->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title-add">Tambah Produk</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="produk_form">
                    <div class="modal-body">
                        <div class="form-group">
                            <label for="nama">Nama</label>
                            <input type="text" class="form-control" name="nama" id="nama"  placeholder="...">
                            <input type="hidden" name="id_produk" id="id_produk">
                        </div>

                        <div class="form-group">
                            <label for="jenis">Jenis</label>
                            <input type="text" class="form-control" name="jenis" id="jenis" placeholder="...">
                        </div>

                        <div class="form-group">
                            <label for="harga">Harga</label>
                            <input type="text" class="form-control" name="harga" id="harga" placeholder="...">
                        </div>

                        <div class="form-group">
                            <label for="deskripsi">Deskripsi</label>
                            <input type="text" class="form-control" name="deskripsi" id="deskripsi" placeholder="...">
                        </div>

                        <div class="form-group">
                            <?php
                            $content = file_get_contents('http://localhost/Magang/rest-server/index.php/api/kategori');
                            $array_data = json_decode($content);
                            ?>
                            <label for="kategori">Kategori</label>
                            <select class="form-control" name="kategori_id" id="kategori_id">>
                                <?php foreach ($array_data->data as  $value) { ?>
                                    <option value="<?= $value->id_kategori ?>"><?= $value->nama_kategori ?></option>
                                <?php } ?>
                            </select>
                        </div>

                        <div class="form-group">
                            
                            <label for="keterangan">Keterangan</label>
                            <textarea class="ckeditor" id="ckeditor" name="keterangan"  rows="3"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="gambar">Gambar</label>
                            <input  class="form-control" type="file" name="gambar" id="gambar">
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary">Simpan</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Delete modal -->
    <div class="modal fade" id="deletemodal" tabindex="-1" role="dialog" aria-labelledby="deletemodal" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">Hapus kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="delete_form">
                    <div class="modal-body">
                        <p>Apakah yakin mengapus kategori <strong id="nama_kategori_delete"></strong> ?</p>
                        <input type="hidden" name="id_produk" id="id_produk_delete">
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                        <button type="submit" class="btn btn-primary">Hapus</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <!-- Detail modal -->
    <div class="modal fade" id="showdata" tabindex="-1" role="dialog" aria-labelledby="showdata" aria-hidden="true">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modal-title">Detail Kategori</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                    <div class="modal-body">
                        <img src="<?=base_url('gambar_produk'.'id_produk')?>" id="gambar_produk_show">
                        <h5>Nama :<strong id="nama_kategori_show"> </strong></h5>
                        <p>Harga : <span style="color: red" id="harga_produk_show"></span></p>
                        <p>Jenis : <span id="jenis_produk_show"></span></p>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Kembali</button>
                    </div>
            </div>
        </div>
    </div>
</div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="http://ajax.aspnetcdn.com/ajax/jquery.dataTables/1.9.4/jquery.dataTables.min.js"></script>

<script type="text/javascript" charset="utf-8">
    $(document).ready(function() {
      $('#table').dataTable();
    } );
</script> 

<script type="text/javascript">

  // <!-- fungsi input produk -->
$('#produk_form').submit(function(e) {
    var form = $(this);
    var url =  "<?= base_url('index.php/api/produk');?>";
    var types="";
    if ($('#id_produk').val() == '') {
        types = "POST";
    }else{
        types = "PUT";
    }
    e.preventDefault();
    $.ajax({
        type: types,
        url: url,
        data:new FormData(this), //penggunaan FormData
         processData:false,
         contentType:false,
         cache:false,
         async:false,
        dataType: "json",
        success: function(data){
          location.reload();
        },
        error: function() { alert("Error data."); }
    });
});

function edit(id, nama){
    var url =  "<?= base_url('index.php/api/produk/')?>"+id;
    // console.log($('#produk_form').serializeArray())
    $.ajax({
        type: 'GET',
        url: url,
        dataType: "json",
        success: function(res){
            if (res.status == 'success') {
                $('#nama').val(res.data.nama)
                $('#id_produk').val(res.data.id_produk)
                $('#jenis').val(res.data.jenis)
                $('#harga').val(res.data.harga)
                $('#deskripsi').val(res.data.deskripsi)
                $('#kategori_id').val(res.data.kategori_id)
                $('#ckeditor').text(res.data.keterangan)
                $('#gambar').val(res.data.gambar)
            }
        },
        error: function() { alert("Error data."); }
    });

    $('#modal-title-add').text('Edit Produk');
    $('#exampleModal').modal('show')
    $('#id_produk').val(id);
}

function hapus(id, nama){
    $('#deletemodal').modal('show')
    $('#nama_kategori_delete').text(nama);
    $('#id_produk_delete').val(id);
}

function show(id, nama, harga, jenis, gambar){
    $('#showdata').modal('show')
    $('#nama_kategori_show').text(nama);
    $('#harga_produk_show').text(harga);
    $('#jenis_produk_show').text(jenis);
    $('#gambar_produk_show').text(gambar);
}

$('#exampleModal').on('hidden.bs.modal', function () {
    $('#modal-title-add').text('Tambah Kategori');
    $('#id_produk').val('');
    $('#nama_kategori').val('');
})

$('#delete_form').submit(function(e) {
    var form = $(this);
    console.log(form.serialize());
    var url =  "<?= base_url('index.php/api/produk');?>";
    e.preventDefault();
    console.log();
    $.ajax({
        type: "DELETE",
        url: url,
        data: form.serialize(),
        dataType: "json",
        success: function(data){
          location.reload();
        },
        error: function() { alert("Error data."); }
    });
});


</script>